import React, { Component } from 'react';
import { gcd } from '../QuickMaffs';

export default class Exercise2 extends Component {
    constructor(props) {
        super(props);

    }

    // For assignment 6
    eulerTotient(x) {
        var resultJsx = [];
        resultJsx.push(<div key={"calcuphi"}>{`Calculating phi(${x}) (Depending on how big number, could be pretty long)`}</div>)

        var primeCounter = 0;
        for(var y = 1; y < x; y++) {
            var gcded = gcd(y, x);
            
            if(gcded === 1) {
                resultJsx.push(<div key={y}>{`gcd(${y}, ${x}) = 1`}</div>)
                primeCounter++;
            }
        }
        resultJsx.push(<div key={"done"}>{`Prime numbers from 1...n: ${primeCounter}`}</div>)
        return resultJsx;
    }

    render() {
        return(
            <div id="exercise-2">
                <h2>Exercise 2</h2>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 1</h3>
                        <div className="code">
{`
Calculate 823^12 (mod 5)
------------------------
823^12 = rem(823,5)^12 = 3^12..

(Whoops, 3^12 splits into (3^2)^6, not (3^3)^9)
3^12 = (3^3)^9 = 27^9..

27^9 = rem(27,5)^9 = 2^9..
2^9 = 512
rem(512, 5) = 2
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 2</h3>
                        <div className="code">
{`
It's practically the same as assignment 1, but longer.

Calculate rem((9943^18181818)^2222 - 7478^1212 * 4889^5678, 18)
------------------------
= 
= rem(9943,18)^18181818)^2222 - rem(7478,18)^1212 * rem(4889,18)^5678 (mod 18)
= 7^18181818)^2222 - 8^1212 * 11^5678 (mod 18)
= (7^18181818)^2222...

= (7^18181818)^2222 - 8^1212 * 11^5678 (mod 18)

Since 8^1212 has an even exponent we can use [Note 2] from below to simplify the exponent.
= (7^18181818)^2222 - 10 * 11^5678

18181818 is divisible by 3, so they belong to the same class.
Therefore we can use [Note 1] to convert.

We do this by doing 18181818 % 3 = 0,
since the remainder is 0 we can look at 7^0 which is 
the same as 7^3 due to Modulo 3.
Looking at [Note 1] we can see 7^3 is 1.
therefore...

= 1^2222 - 10 * 11^5678
= 1 - 10 * 11^5678

... The "pattern" of 11^5678 is in [Note 3]

if we take 5678 % 6, we get a remainder of 2.

6+2=8, so we can do 11^8 which is 13.

Since 11^8 is the same as 11^2 (due to Modulo 6), we
can also look at that as well.

Would have been easier to just  use the remainder from
above and go straight for 11^2.

= 1 - 10 * 13
= -129 % 18 = -3

> Since we're in negative
= -129 % 18 = -3 (mod 18)
= 15 (mod 18)

------------------------
Notes to support calculations are below.
Note 1:
rem(7^1, 18) = 7
rem(7^2, 18) = 13 
rem(7^3, 18) = 1
rem(7^4, 18) = 7
rem(7^5, 18) = 13
rem(7^6, 18) = 1
rem(7^7, 18) = 7
rem(7^8, 18) = 13 
rem(7^9, 18) = 1
rem(7^10, 18) = 7
rem(7^11, 18) = 13 
rem(7^12, 18) = 1
---
Note 2:
8^1 % 18 = 8
8^2 % 18 = 10
8^3 % 18 = 8
8^4 % 18 = 10
---
Note 3:
11^1 % 18 = 11
11^2 % 18 = 13
11^3 % 18 = 17
11^4 % 18 = 7
11^5 % 18 = 5
11^6 % 18 = 1
11^7 % 18 = 11
11^8 % 18 = 13
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 3</h3>
                        <div className="code">
{`
a)  Calculate phi(1155)
---------------------
Using Note 1 below...
(This method only works if no repeated prime factors)
phi(1155) = 1155 * (1-1/3) * (1-1/5) * (1-1/7) * (1-1/11)
          = 1155 * 2/3 * 4/5 * 6/7 * 10/11
          = 1155 * (2*4*6*10)/(3*5*7*11)
          = 1155 * (8*60)/(15*77)
          = 1155 * 480/1155
          = (1155 * 480)/1155
          = 554 400/1155
          = 480
---------------------
b) Kinda trick question, just have to do phi(14014):
---------------------
phi(14014) = phi(2) * phi(7^2) * phi(11) * phi(13)
           = 1 * (7^2 - 7) * 10 * 12
           = 1 * 42 * 10 * 12
           = 42 * 120 = 5040
Notes below.
------
[Note 1]:
Prime-factors of 1155:
1155 / 3 = 385
385 / 5 = 77
77 / 7 = 11
11 / 11 = 0
Factors: 3, 5, 7, 11

[Note 2]:
Prime factors of 14014:
14014 / 2 = 7007
7007 / 7 = 1001
1001 / 7 = 143
143 / 11 = 13
13 / 13 = 0
Factors: 2,7,7,11,13
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 4</h3>
                        <div className="code">
{`
a) Calculate rem(21^22, 23)
---------------------
Since 23 is prime it divides by anything else,
according to Fermat's little theorem, x^(p-1) === 1 (mod p)
so the answer is simply 1.


b) Calculate rem(19^10081, 14014)
---------------------
= rem(19^5040 * 19^5040 * 19, 14014)

We use Euler's theorem that says k^phi(n) = 1 (mod n).
We can do this because phi(14014) = 5040.
= rem(1 * 19^5040 * 19, 14014)
= rem(1 * 1 * 19, 14014)
= rem(19, 14014)
= 19 % 14014 = 19
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 5</h3>
                        <div className="code">
{`
Calculate the multiplicative inverse (mod 29) of 9

First we use Euclid's extended algorithm (tb + sa = 1) where a = 9, b = 29

29 = 3 * 9 + 2
9 = 4 * 2 + 1
2 = 2 * 1 + 0
so gcd(9,29) = 1

now the "extended" part:
29 = 3*9 + 2
    so 2 = 29 - 3 * 9
9 = 4*2 + 1
    so 1 = 9 - 4*2
         = 9 - (4 * (29 - 3 * 9))
         = 9 - (4 * 29 - 12 * 9)
         = 13 * 9 - 4 * 29

2 = 2*1 + 0

so "s" in (tb + sa = 1) in this case is 13, so the answer is 13.
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 6</h3>
                        <div className="code">
                            {this.eulerTotient(1155)}
                        </div>
                    </span>
                </div>
            </div>
        )
    }
}
import React, { Component } from 'react';
import uppg5_5_tree from "../pictures/uppg5_5_tree.jpg";

export default class Exercise5 extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div id="exercise-5">
                <h2>Exercise 5</h2>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 1</h3>
                        <div className="code">
                        {`
Let's first calculate how likely it is to get different amounts of sixes with the dice:
Pr[0 sixes] = (5/6)^3 = (5*5*5) over (6*6*6) = 125 over 216
Pr[1 sixes] = (1 over 6) * (5 over 6)^2 * (3 choose 1) 
            = 1 over 6 * 25 over 36 * 3
            = 25 over (36*6) * 3
            = 25 over 216 * 3
            = (25*3) over (216*3)
            = 75 over 216
                The 1 over 6 is there because of the 6 you'd get.
                The (5 over 6)^2 is there because they're the other numbers.
                The (3 choose 1) is needed because the 6 we get could be either
                of the 3 dice.

Pr[2 sixes] = (1 over 6)^2 * (5 over 6) * (3 choose 2)
        = 1/36 * 5/6 * 3
        = 5/216 * 3
        = 15/216

Pr[3 sixes] = (1 over 6)^3
            = 1 over (6*6*6)
            = 1/216
                The reason we don't need 3 choose 3 here is because the dice will all be 6 so order doesn't matter.

In order to find out how much money you'd win/lose by average we set it up like this:
Where "k" is some until further unknown amount of money.
(PrNumerator[0 sixes] * -1€ + PrNumerator[1 sixes] * 1€ + PrNumerator[2 sixes] * 2€ + PrNumerator[3 sixes] * k€) over 216
= (125 * -1€ + 75 * 1€ + 15 * 2€ + 1 * k€) over 216

So in other words we want to find the value of "k" where the result of the parenthesized section will equal 0.

0/216 = (125 * -1€ + 75 * 1€ + 15 * 2€ + 1 * k€)
        = -125 + 75 + 30 + 1 * k€
        = -20 + 1 * k€
        = -20 + 20

The answer is thus 20.
                            `}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 2</h3>
                        <div className="code">
                        {`
You *should* use a tree for this I think..

Pr[Marina] = 3/12
Pr[Mojgan] = 4/12
The possible events in general are:
(Ma, Mo)
(-Ma, Mo)
(Ma, -Mo)
(-Ma, -Mo)

a)
The events where at least one person succeeds are:
(Ma, Mo)
Pr[(Ma, Mo)] = 3/12 * 4/12 = 12/144

(-Ma, Mo)
Pr[(-Ma, Mo)] = 9/12 * 4/12 = 36/144

(Ma, -Mo)
Pr[(Ma, -Mo)] = 3/12 * 8/12 = 24/144

Pr[(Ma, Mo)] + Pr[(-Ma, Mo)] + Pr[(Ma, -Mo)] = 12/144 + 36/144 + 24/144 = 72/144

The events where Mojgan learns to fly are: (-Ma, Mo) and (Ma, Mo), and the probability of those events are:
36/144 + 12/144 = 48/144

So the probability of Mojgan learning to fly when at least one of you succeed is:
(48/144)/(72/144) = (48/144) * (144/72) = 6912/10368 = 2/3 (Since 6912/2 = 3456, and we divide both the bigger numbers with that.)

b)
All possible event probabilities where at most one person succeeds:
Pr[(-Ma, Mo)] + Pr[(Ma, -Mo)] + Pr[(-Ma, -Mo)]
= (9/12 * 4/12) + (3/12 * 8/12) + (9/12 * 8/12)
= 36/144 + 24/144 + 72/144
= 132/144

And now the ones where it's "+Ma".
Pr[(Ma, -Mo)] = 3/12 * 8/12 = 24/144

So the probability of Marina learning to juggle chainsaws when at most one of you succeed is..
= (24/144)/(132/144)
= (24/144) * (144/132)
= 3456/19008
= 2/11 (because 3456/2 = 1728, and 1728 also divides 19008 into 11)

c)
Scenarios where exactly one of you succeed:
(Ma, -Mo)
(-Ma, Mo)

Pr[(Ma, -Mo)] + Pr[(-Ma, Mo)]
= (3/12 * 8/12) + (9/12 * 4/12)
= (24/144) + (36/144)
= 60/144

So since we want to check how likely it is for "+Mo",
we do:

Pr[(-Ma, Mo)] / 60/144
= 36/144 / 60/144
= 36/144 * 144/60
= 5184/8640
= 3/5 (Because 5184/3 = 1728, and 8640/1728 = 5)
                            `}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 3</h3>
                        <div className="code">
                        {`
a)
Pr[3 of 14 packets have errors]
= (14 choose 3) * (1/5)^3 * (4/5)^11
(We need the 14 choose 3 because we need to account for the packets in different orders)
= (14! / 11! * 3!) * 0.2^3 * 0.8^11
= (87 178 291 200 / 39 916 800 * 6) * 0.2^3 * 0.8^11
= (87 178 291 200 / 239 500 800) * 0.2^3 * 0.8^11
= (87 178 291 200 / 239 500 800) * 0.2^3 * 0.8^11
= 364 * 0.008 * 0.086
= ~0.250

b)
Pr[< 3 of 14 packets have errors]

We basically have to calculate:
Pr[0 of 14 packets have errors]
+
Pr[1 of 14 packets have errors]
+
Pr[2 of 14 packets have errors]

Pr[0 of 14 packets have errors]
= (4/5)^14
= 0.8^14
= 0.044

Pr[1 of 14 packets have errors]
= (14 choose 1) * (1/5) * (4/5)^13
= 14 * 0.2 * (0.8)^13
= 14 * 0.2 * ~0.055
= ~0.154

Pr[2 of 14 packets have errors]
= (14 choose 2) * (1/5)^2 * (4/5)^12
= (87 178 291 200 / 2! * 12!) * (1/5)^2 * (4/5)^12
= (87 178 291 200 / 2 * 479 001 600) * (1/5)^2 * (4/5)^12
= (87 178 291 200 / 958 003 200) * (1/5)^2 * (4/5)^12
= 91 * (1/5)^2 * (4/5)^12
= 91 * 0.2^2 * 0.8^12
= 91 * 0.04 * ~0.069
= ~0.251

So...
0.044 + 0.154 + 0.251 = 0.449

c)
We just need the complement of:
(Pr[3 of 14 packets have errors] Union Pr[< 3 of 14 packets have errors])

C = -(Pr[3 of 14 packets have errors] Union Pr[< 3 of 14 packets have errors])
C = 1 - (Pr[3 of 14 packets have errors] + Pr[< 3 of 14 packets have errors])
C = 1 - (0.250 + 0.449)
C = 1 - (0.699)
C = 0.301
so the answer is ~0.301
                            `}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 4</h3>
                        <div className="code">
                        {`
We assume that the algorithm is sorting a worst-case array.
Let's say we have the array [4,3,2,1] (n=4) and we want it in ascending, not descending order

So it would go like this:
[(4),3,2,1] <- 1 comparison(to the left)
[4,(3),2,1] <- 3 selected
[(3),4,2,1] <- 3 is compared 1 time(with the 4) and inserted before the 4
[3,4,(2),1]
[(2),3,4,1] <- 2 is compared 2 times(with 4 and then 3)
[2,3,4,(1)]
[1,2,3,4] <- 1 is compared 3 times (with 4,3,2)

In this case there were 5 i.e. n+2 comparisons in the inner loop and thus in total (n^2)+2 comparisons

In a *best* case scenario we already have a sorted array, such as [1,2,3,4].

In this case there are n inner comparisons as each number just compares once to their left once.
So here there are just n many comparisons inside the for loop.

So I guess we should in average, including the outer loop, expect n^2 comparisons,
as one array-item out of place can cause a massive increase in comparisons later on.

IN OTHER, MORE PROOFY WORDS:
The inner loop, as we concluded in the rambling above, in a worst case scenario, does
n comparisons(well, actually n+1)
So let's say z=n+1

Now, there's a 50/50 chance of either of the conditions happening in the while-loop so:

SUM(p=0..n-1): p+1 * 0.5

Since we have the +1 in there we can change this to
SUM(p=1..n): p * 0.5

And since the 0.5 is applied to all of the summed numbers anyways,
we can just apply it to the final number:
0.5 * SUM(p=1..n): p

Since this is an arithmetic sum we can change its form:
(I *think* this is correct)

= (p * (p + 1))/2

The reason we have the +1 is because we start from p=1
                            `}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 5</h3>
                        <div className="code">
                        {`
So what we want to calculate first is E[A] and E[F]

a) Calculate E[A]:
E[A] = E[A1] + E[A2] + E[A3]

E[A1] = (0.75 * 2) + (0.75 * 2) + ... + (0.75 * 2).. 10 times
=  (0.75 * 2) * 10
= 1.5 * 10
= 15

E[A2] =
We can think of the two dice as one 12-sided die.

Due to linearity of the probabilities:
E[A2'] = ..
= (E[1 die-throw] * 2) + 3
= 3.5 * 2 + 3
= 7 + 3
= 10

E[A2] = ..
= 10 * 4
= 40

E[A3] = ..
(0.5 * 12 + 0.5 * 18) = ..
= 6 + 9
= 15

So E[A] i.e. the answer to part (a) = 15 + 40 + 15 = 70


E[F] = ..
= E[1 die-throw] * E[1 die-throw] + E[F']

E[F'] = ..
= 0.4 * 40 + 0.3 * 50 + 0.3 * 60
= 16 + 15 + 18
= 31 + 18
= 49

So..
E[F] (and the answer to (b))= 3.5 * 3.5 + 49
= 61.25

To calculate part c)
We simply do:
Pr[A] * E[A] + Pr[F] * E[F] + Pr[Lost] * 84
= 4/7 * 70 + 2/7 * 61.25 + 1/7 * 84
= 280/7 + 122.5/7 + 84/7
= 40 + 17.5 + 12
= 52 + 17.5
= 69.5
`}
                        </div>
                    </span>
                </div>
                <div>
                    <h4>5-5 Tree</h4>
                    <img width={600} src={uppg5_5_tree} alt="uppg5_5_tree" />
                </div>
            </div>
        )
    }
}
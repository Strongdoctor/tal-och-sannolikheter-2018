import React, { Component } from 'react';
import { gcd } from '../QuickMaffs';

export default class Exercise1 extends Component {
    constructor(props) {
        super(props);

    }

    // For assignment 1
    euclidsWithExplanations(a,b) {
        // Check equality just in case
        if(a === b) {
            return a;
        }

        // Re-order so that a > b
        if(a < b) {
            var c = b;
            b = a;
            a = c;
        }

        var finalResultJsx = [];
        var rem;

        while(b > 0) {
            finalResultJsx.push(<div>gcd({a},{b}) =</div>)
            rem = a % b;
            finalResultJsx.push(<div>gcd({b},rem({a},{b})) =</div>)
            a = b;
            b = rem;
        }
        finalResultJsx.push(<div><strong>{a}</strong></div>)
        return finalResultJsx;
    }

    render() {
        return(
            <div id="exercise-1">
                <h2>Exercise 1</h2>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 1</h3>
                        <div className="code">
                            {this.euclidsWithExplanations(819,651)}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 2</h3>
                        <div className="code">
                        {`
a = 405, b = 342

405 = 1 * 342 + 63  so.. 63 = 1 * 405 + -1 * 342
342 = 5 * 63 + 27   so.. 27 = 1 * 342 + -5 * 63
                            = 1 * 342 + -5 * (1 * 405 + -1 * 342)
                            = 1 * 342 + (-5 * 405 + 5 * 342)
                            = -5 * 405 + 6 * 342

63  = 2 * 27 + 9    so.. 9  = 1 * 63 + -2 * 27
                            = 1 * 63 + -2 * (-5 * 405 + 6 * 342)
                            = 1 * (1 * 405 + -1 * 342) + -2 * (-5 * 405 + 6 * 342)
                            = 1 * 405 + -1 * 342 + -2 * (-5 * 405 + 6 * 342)
                            = 1 * 405 + -1 * 342 + 10 * 405 + -12 * 342
                            = 11 * 405 + -13 * 342
                            so s = 11, t = -13

27 = 3 * 9 + 0      so.. gcd(405, 342) = 9
                        `}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 3</h3>
                        <div className="code">
                        {`
Part 1:
First we do gcd(24,9) = 3.
a = 24, b = 9
gcd(24,9) =
gcd(9, 24 % 9) =
gcd(6, 9 % 6) =
gcd(3, 6 % 3) = .. = 3

You can't get 8 by multiplying with 3.

Part 2:
First we run gcd(28,19) = 1.
a = 28, b = 19
gcd(28, 19) =
gcd(19, 28 % 19) =
gcd(9, 19 % 9) =
gcd(1, 9 % 1) = 0 .. = 1

You can get 5 by multiplying with 1.
                        `}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 4</h3>
                        <div className="code">
                        {`
96 / 2 = 48
48 / 2 = 24
24 / 2 = 12
12 / 2 = 6
6 / 2 = 3
3 / 3 = 0
Prime factors: 2, 2, 2, 2, 2, 3

374 / 2 = 187
187 / 11 = 17
17 / 17 = 0
Prime factors: 2, 11, 17

1925 / 5 = 385
385 / 5 = 77
77 / 7 = 11
11 / 11 = 0
Prime factors: 5, 5, 7, 11

54978 / 2 = 27489
27489 / 3 = 9163
9163 / 7 = 1309
1309 / 7 = 187
187 / 11 = 17
17 / 17 = 0
Prime factors: 2, 3, 7, 7, 11, 17

508079 / 11 = 46189
46189 / 11 = 4199
4199 / 13 = 323
323 / 17 = 19
19 / 19 = 0
Prime factors: 11, 11, 13, 17, 19
                        `}
                        </div>
                    </span>
                </div>
            </div>
        )
    }
}
import React, { Component } from 'react';
import { gcd } from '../QuickMaffs';
import uppg4_2_tree from "../pictures/uppg4_2_tree.jpg";
import uppg4_2c_tree from "../pictures/uppg4_2c_tree.jpg";
import uppg4_3_tree from "../pictures/uppg4_3_tree.jpg";


export default class Exercise4 extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div id="exercise-4">
                <h2>Exercise 4</h2>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 1</h3>
                        <div className="code">
{`
a)
We just do Euclid's extended algorithm resulting in sa + tb where a is 502, b is 11, and we want to find t.

502 = 45 * 11 + 7
    so 7 = 502 + (-45 * 11)
11 = 1 * 7 + 4
    so 4 = 11 - 7
         = 11 - (502 + (-45 * 11))
         = 11 - (502 - 45 * 11)
         = -1 * 502 + 46 * 11

7 = 1 * 4 + 3
    so 3 = 7 - 4
         = 7 - (-1 * 502 + 46 * 11)
         = 502 + (-45 * 11) - (-1 * 502 + 46 * 11)
         = 1 * 502 + (-45 * 11) - (-1 * 502 + 46 * 11)
         = 2 * 502 + -45 * 11 - 46 * 11
         = 2 * 502 + -45 * 11 + -46 * 11
         = 2 * 502 + -91 * 11

4 = 1 * 3 + 1
    so 1 = 4 - 3
         = -1 * 502 + 46 * 11 - 2 * 502 + -91 * 11
         = -3 * 502 + 46 * 11 + 91 * 11
         = -3 * 502 + 137 * 11

3 = 3 * 1 + 0 so we're done here.

so the multiplicative inverse of 11 (mod 502) is 137.

b)
Same as previous but a=58, b=13
58 = 4 * 13 + 6
    so 6 = 1 * 58 + -4 * 13

13 = 2 * 6 + 1
    so 1 = 13 - 2 * 6
         = 13 - 2 * (58 + -4 * 13)
         = 13 - 2 * 58 + 8 * 13
         = -2 * 58 + 9 * 13

6 = 6 * 1 + 0...

So the multiplicative inverse of 13 (mod 58) is 9.
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 2</h3>
                        <div className="code">
{`
(Tree to accompany this assignment is in the bottom of this page)

a)
From the assignment we could find 3 different ways the entire thing would end:
1. Bane & Sauron are released
2. Bane & Kylo are released
3. Kylo & Sauron are released

We then check how likely it is that Bane will be released based on what the guard says, in each of the 3 scenarios.


(Note for self: you can find the "Conversion Formula" for this on page 6 in PDF 12. Betingad Sannolikhet)
In the tree we see that Pr[B|VS] =...
Pr[B intersection VS]/Pr[VS] = Pr[{(B,S),VS}]/(1/3 + 1/6) = (1/3)/(3/6) = (1/3)*(6/3) = 2/3

b)
In the tree we see that Pr[B] in general is 1/3 + 1/3 = 2/3

c)
(The modified tree diagram in bottom of page)

Pr[B|VS] = Pr[B intersection VS]/Pr[VS]
= Pr[{(B,S), VS}]/(2/3)
= (1/3)/(2/3)
= 1/3 * 3/2
= 3/6
= 1/2

Pr[B|VK] = 1, as the only time VK will be said, is when Bane also will be released,
i.e. when (B,VK) happens.
In other words, when the guard says VK, the probability of Bane being release is 100%.

d)
Pr[B] = Pr[B|VS] * VS + PR[B|VK] * VK
      = 2/3 * 3/6 + 1 * 1/3
      = 6/18 + 1/3
      = 1/3 + 1/3
      = 2/3
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 3</h3>
                        <div className="code">
{`
(Poorly drawn tree of this in the bottom of page)
a)
If we quickly draw up a tree we can see that there are 2^3 = 8 possible outcomes.

Pr[A] is 1/8 + 1/8 + 1/8 + 1/8 = 4/8 = 1/2
Pr[B] is also 1/2
Pr[C] is.. also 1/2
Pr[D] is highlighted in the tree, 1/8 + 1/8 + 1/8 = 1/2

A:
{H H H}
{H T H}
{H T T}
{H H T}

B:
{H H H}
{T H H}
{T H T}
{H H T}

C:
{H H H}
{T T H}
{T H H}
{H T H}

D:
{H H T}
{H T H}
{T H H}
{T T T}

b)
We can at least say that A,B and C are mutually independent, just from thinking.

To *disprove* that all of the events are mutually independent, we'll show that Pr[A intersection B intersection C intersection D] != Pr[A] * Pr[B] * Pr[C] * Pr[D].
NOTE! We might as well just show Pr[A intersection D] != Pr[A] * Pr[D].

A intersection B intersection C intersection D:
{0}

And Pr[{0}] = 0.

Pr[A] * Pr[B] * Pr[C] * Pr[D] = 1/2 * 1/2 * 1/2 * 1/2 = 1/16

So they are NOT mutually independent, since 0 != 1/16.

c)
Pr[A ins B ins C] = Pr[{H H H}] = 1/8
Pr[A] * Pr[B] * Pr[C] = 1/2 * 1/2 * 1/2 = 1/8.. checks out!

Pr[A ins B ins D] = Pr[{H H T}] = 1/8
Pr[A] * Pr[B] * Pr[D] = 1/2 * 1/2 * 4/8 = 4/32 = 1/8.. checks out!

Pr[B ins C ins D] = Pr[{T H H}] = 1/8
Pr[B] * Pr[C] * Pr[D] = 1/2 * 1/2 * 1/2 = 1/8.. checks out!

and lastly..

Pr[A ins C ins D] = Pr[{H T H}] = 1/8
Pr[A] * Pr[C] * Pr[D] = 1/2 * 1/2 * 1/2 = 1/8.. checks out!

Now we've proven that A, B, C and D are 3-way independent, but not mutually independent.
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 4</h3>
                        <div className="code">
                            Can't find time to do it ¯\_(ツ)_/¯
                        </div>
                    </span>
                </div>
                <div>
                    <h4>4-2 Tree</h4>
                    <img width={600} src={uppg4_2_tree} alt="uppg4_2_tree" />
                </div>
                <div>
                    <h4>4-2c Tree</h4>
                    <img width={600} src={uppg4_2c_tree} alt="uppg4_2c_tree" />
                </div>
                <div>
                    <h4>4-3 Tree</h4>
                    <img width={600} src={uppg4_3_tree} alt="uppg4_3_tree" />
                </div>
                
            </div>
        )
    }
}
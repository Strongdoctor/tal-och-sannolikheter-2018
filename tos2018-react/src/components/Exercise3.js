import React, { Component } from 'react';
import { gcd } from '../QuickMaffs';
import uppg3_3b_tree from "../pictures/uppg3_3b_tree.jpg";
import uppg3_4_tree from "../pictures/uppg3_4_tree.jpg";

export default class Exercise3 extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div id="exercise-3">
                <h2>Exercise 3</h2>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 1</h3>
                        <div className="code">
{`
p = 13, q = 17
n = p * q = (13 * 17) = 221
n' = p-1 * q-1 = 192

Calculating e:
First we prime-factorize n'
192 / 2 = 96
96 / 2 = 48
48 / 2 = 24
24 / 2 = 12
12 / 2 = 6
6 / 2 = 3
3 / 3 = 0

Prime factors: 6x 2, 3
We list some integers from [2..n']:
2,3,4,5,6,7,8,9,10,11
We remove any multiples of 2 and 3, i.e. the prime factors of n' and then we're left with:
5,7,11

We choose one of these at random, let's take 7.
e = 7

Now let's calculate our private key called d.

Euclids extended algorithm b = 192, e = 7, will calculate sb + te

192 = 27 * 7 + 3
    so.. 3 = 192 - 27 * 7
7 = 2 * 3 + 1
    so.. 1 = 7 - 2 * 3
           = 7 - 2 * (192 - 27 * 7)
           = 7 - (2 * 192 - 54 * 7)
           = -2 * 192 + 55 * 7
    
    so d is 55!
--------------
Now on to decrypting the message I got:
n = 221
d = 55
m* = 54

Please tell me if there's a quicker, or easier way to do the stuff below:
m = (54)^55 (mod 221)
  = (54^5)^11
  = (459165024)^11
  = rem(459 165 024, 221)^11
  = 175^11
  = 164 130 859 375 * 28 722 900 390 625
  = rem(164 130 859 375, 221) * rem(28 722 900 390 625, 221)
  = 184 * 155
  = 28 520 (mod 221)
  = 11 (mod 221)
  = "Why is the rum gone?"
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 2</h3>
                        <div className="code">
{`
a)
First we prime-factorize 4370, per the assignment description:
4370 / 2 = 2185
2185 / 5 = 437
437 / 19 = 23
23 / 23 = 0

So prime factors are 2, 5, 19, 23

On to part 2:
phi(2) * phi(5) * phi(18) * phi(23)
Since they are all prime numbers:
1 * 4 * 17 * 22
= 1496

b)
Calculate phi(359513):
First we prime-factorize it:
359 513 / 7 = 51 359
51 359 / 7 = 7337
7 337 / 11 = 667
667 / 23 = 29
29 / 29 = 0

Prime factors: 7, 7, 11, 23, 29

= phi(7^2) * phi(11) * phi(23) * phi(29)
= (7^2 - 7) * 10 * 22 * 28
= 42 * 10 * 22 * 28
= 258720
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 3</h3>
                        <div className="code">
{`
a)
Since we want all of the 4 screws not to be broken, there is only 1 possible combination:
(W W W W) where W means whole(i.e. not broken),
i.e. (4 over 4) = 1
60/64 * 59/63 * 58/62 * 57/61
= (60*59*58*57)/(64*63*62*61)
= 11 703 240 / 15 249 024
= ~76,7%

b)
Built a tree of this, available in /src/pictures/uppg3-3b_tree.jpg and at the very bottom of this page.

The chance of the second number being even is:
2/20 + 6/20 = 8/20 = 2/5

c)
Note:
First count the total amount of possible results.
Then count all the Good events.

The answer is n + 1 and I don't know why.
`}
                        </div>
                    </span>
                </div>
                <div className="assignment-part">
                    <span>
                        <h3>Assignment 4</h3>
                        <div className="code">
{`
Tree I used to calculate this is in the source-code, the /src/pictures/ folder and also at the very bottom of this page.

a) 
The 1/1 probability means there wasn't a need for a third game.
18/125 + 12/125 + 18/125 + 12/125 = ..

60/125 <--- the answer

b)
18/125 + 12/125 = 30/125 = ..

6/25 <--- the answer

c)
Let's choose TPS' chances of winning, just to entertain the thought:
45/125 + 18/125 + 18/125 = .. 

81/125 <--- the answer
`}
                        </div>
                    </span>
                </div>
            <div>
                <h4>3-3b Tree</h4>
                <img width={600} src={uppg3_3b_tree} alt="uppg3_3b_tree" />
            </div>
            <br/>
            <div>
                <h4>3-4 Tree</h4>
                <img width={600} src={uppg3_4_tree} alt="uppg3_4_tree" />
            </div>
            </div>
        )
    }
}
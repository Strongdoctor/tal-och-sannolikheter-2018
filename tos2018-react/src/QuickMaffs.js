export function gcd(a,b) {
    // Check equality
    if(a === b) {
        return a;
    }

    // Re-order so that a > b
    if(a < b) {
        var c = b;
        b = a;
        a = c;
    }

    while(b > 0) {
        var rem = a % b;
        a = b;
        b = rem;
    }

    return a;
}
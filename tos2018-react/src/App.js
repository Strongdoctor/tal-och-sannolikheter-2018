import React, { Component, Fragment } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from "react-router-dom";
import Exercise1 from './components/Exercise1';
import Exercise2 from './components/Exercise2';
import Exercise3 from './components/Exercise3';
import Exercise4 from './components/Exercise4';
import Exercise5 from './components/Exercise5';
import PageNotFound from './components/PageNotFound';
import Home from './components/Home';

class App extends Component {
  render() {
    return (
      <Router>
        <Fragment>
          <div className="App">
            <header className="App-header">
              <h1><Link to="/">Tal och Sannolikheter 2018</Link></h1>
              <nav>
                <ul>
                  <li>
                    <Link to="/exercise1">Exercise 1</Link>
                  </li>
                  <li>
                    <Link to="/exercise2">Exercise 2</Link>
                  </li>
                  <li>
                    <Link to="/exercise3">Exercise 3</Link>
                  </li>
                  <li>
                    <Link to="/exercise4">Exercise 4</Link>
                  </li>
                  <li>
                    <Link to="/exercise5">Exercise 5</Link>
                  </li>
                </ul>
              </nav>
            </header>
            <main>
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/exercise1" exact component={Exercise1} />
                <Route path="/exercise2" exact component={Exercise2} />
                <Route path="/exercise3" exact component={Exercise3} />
                <Route path="/exercise4" exact component={Exercise4} />
                <Route path="/exercise5" exact component={Exercise5} />
                <Route component={PageNotFound} />
              </Switch>
            </main>
            <footer>
              Source code is{" "}
              <a href="https://gitlab.com/Strongdoctor/tal-och-sannolikheter-2018.git">
                here
              </a>
            </footer>
          </div>
        </Fragment>
      </Router>
    );
  }
}

export default App;
